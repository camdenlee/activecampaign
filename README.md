# ActiveCampaign Coding Challenge

Demo can be viewed here: [https://activecampaign-challenge.camdenlee.com/](https://activecampaign-challenge.camdenlee.com/)

## Assumptions

**When developing the application, I used the following assumptions:**

- The API didn't have much information about locations. In reading the documentation, it seems like `geoIps` or `geoAddress` would contain location information, but I was unable to access this information. I attempted to request data via sideloading and requests to the `geoIps` endpoint, but nothing returned. Since the exercise mentioned the data could be incomplete, I left this blank by displaying a dash.
- It was a bit difficult to understand what Total Value meant in the instructions. There wasn't a clear direction for where to access that data. In the end, I used the total sum of the deals for each contact.
- Since I was unable to access location data, I implemented a solution that converted all deal values to USD.
- The challenge mentioned that sorting, filtering, or any actions do not need to be included. I decided to omit the last column of the table with actions and the button. This column seemed directly tied to row selection and other actions that were out of scope. I did however include a non-functional checkbox for visual aid as it was an easy component to create.
- There didn't seem to be much information about the avatars in the API response, aside from a `gravatar` parameter in the contact object that contained a value of `1` or `0`. Because of this, I implemented a simple avatar based on the initials of the contact for all contacts.
- I assumed that for every tag within `contact.tag`, there was a corresponding `contactTag` and `tag` with matching IDs. Given additional time, I would account for edge cases where this assumption is not true.
- I assumed that for every deal within `contact.deals` there was a corresponding `deal` with that id. Additionally, I assumed that all deals would include a value and currency. Given additional time, I would account for edge cases where this assumption is not true.

**I also used these opinionated decisions:**

- I used the `useState` hook to hold my state. Given the scale of this app, it didn't necessitate anything more complex.
- I tried to limit additional asynchronous requests where possible to provide the best performance. Because of this, I used sideloading via the API to access tags and deals. This did have side effects though, including response data in a more complex way.
- I also tried to limit iterative functions in processing data to provide the best performance. Instead of looping through `contactTags`, `tags`, and `deals` in `utils/processContacts.js` multiple times, I converted the arrays to objects with key value pairs that would allow a lookup of O(1).
- When thinking about currency conversion rates for deal values, I thought about the changing nature of rates over time. I decided to use an external API to access conversion rate data instead of hardcoding a solution to convert values. I used [https://exchangeratesapi.io/](https://exchangeratesapi.io/) to access this data. Since the rates change, I request new exchange rates each time the dashboard processes API data to ensure the rates are up to date.

## Areas of improvement

Given additional time with the exercise

- Adding a pagination feature would be a nice addition to the exercise.
- As mentioned above, I used the assumption that tags and deals data fits matched directly to the users. While this expectation should be true, ideally I'd spend some time accounting for edge cases.
- react-table was a blessing and a curse. When defining columns in react-table, I had to turn off two standard eslint rules: `react/prop-types` and `react/display-name`. These have to be disabled to follow the react-table implementation of `Cell` and `Header` rendering. I wasn't able to resolve this during this exercise, but with additional time. It'd be worth digging in more.
- Some of the `utils` functions don't account for edge cases. One example is the `arrayToObject` function. This function assumes that all items within the array contain a value with the associated key. Handling situations where this isn't necessarily true would be a great next step.
- Responsive design took a backseat with this exercise. I added a simple wrapper to the `<Table>` that allows the table to scroll side to side, in lieu of the window scrolling.
- Creating custom styles for the checkbox to more closely match the style guide.

## Technical notes

The application was developed from scratch with a custom Webpack installation developed for a previous [Active Campaign coding challenge](https://active-campaign.camdenlee.com/). The following features were added to Webpack to create a build environment:

- ESLint and stylelint for linting and code quality
- Jest for unit testing

The table was created with react-table v7, which uses a hooks implementation. Throughout the application, I used styled-components to add CSS to elements.

## Accessibility notes

When developing the application, I used some accessibility testing tools to check for issues. Those tools include:

- eslint-plugin-jsx-a11y
- [Lighthouse](https://developers.google.com/web/tools/lighthouse)
- [axe - Web Accessibility Testing Chrome Plugin](https://chrome.google.com/webstore/detail/axe-web-accessibility-tes/lhdoppojpmngadmnindnejefpokejbdd?hl=en-US)
- [WAVE Evaluation Tool Chrome Plugin](https://chrome.google.com/webstore/detail/wave-evaluation-tool/jbbplnpkjmmeebjpijfedlgcdilocofh)

## Lighthouse score

I ran the application through Lighthouse for basic benchmarks and accessibility tests. The only issue flagged by lighthouse is the color contrast noted above.

![Lighthouse score](lighthouse.png)

## Challenge Overview

Create an app that fetches a list of contacts and displays them in a table according to our style guide

**Requirements:**

- Use our API and React to create your app
- Follow the style guide
- Submit links to your live app and source code for review
- Write tests for your solution if necessary
- Bonus points: if you use Typescript in your challenge

**Design information:**

You can find the design specification for a ​Table​ in the style guide. Please display the contact name, contact tags, deals, total value with currency and location, as your columns.

You do not need to implement sorting, filtering, or any actions that are depicted in the style guide; those are merely for reference.

**Notes**
- Please provide a link to your live app, this can be CodeSandbox, CodePen, GH pages, self-hosted, etc.
- If you encounter a CORS issues we recommend using ​cors-anywhere​ as a workaround
- The purpose of the exercise is to not only to write a simple app but to have code to discuss and refactor. If you join us in person you'll work with our team to review your code and add a feature together.
- The data on this endpoint is test data and can appear incomplete
- Make your best assumptions and/or record them to help us understand your solution better.
