import arrayToObject from '../../utils/arrayToObject';

describe('test cases for arrayToObject()', () => {
  test('Standard transformation should work', () => {
    const arr = [
      {
        id: 123,
        value: 'lorem',
      },
      {
        id: 234,
        value: 'ipsum',
      },
    ];

    expect(arrayToObject(arr, 'id')).toEqual({
      123: {
        id: 123,
        value: 'lorem',
      },
      234: {
        id: 234,
        value: 'ipsum',
      },
    });
  });
});
