import abbreviateName from '../../utils/abbreviateName';

describe('test cases for abbreviateName()', () => {
  test('A standard two word full name should return a two character string', () => {
    expect(abbreviateName('Chloe Carter')).toBe('CC');
    expect(abbreviateName('Trevor Delgado')).toBe('TD');
    expect(abbreviateName('Anita Tyler')).toBe('AT');
    expect(abbreviateName('Jacquelyn Gonzalez')).toBe('JG');
    expect(abbreviateName('Jasmine Harrison')).toBe('JH');
  });

  test('A one word full name should return one character', () => {
    expect(abbreviateName('Sabrina')).toBe('S');
    expect(abbreviateName('David')).toBe('D');
    expect(abbreviateName('Alex')).toBe('A');
  });

  test('A three or more word full name should only return the first two characters', () => {
    expect(abbreviateName('Tarryn Campbell Gillies')).toBe('TC');
    expect(abbreviateName('John Monroe Smith')).toBe('JM');
    expect(abbreviateName('Lorem Ipsum Dolor Sit Amet')).toBe('LI');
  });

  test('A blank name should return a blank string', () => {
    expect(abbreviateName('')).toBe('');
  });
});
