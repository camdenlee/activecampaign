import formatMoney from '../../utils/formatMoney';

describe('Test cases for formatMoney()', () => {
  test('numbers should be formatted correctly and include dollar sign', () => {
    expect(formatMoney(1)).toBe('$0.01');
    expect(formatMoney(12301)).toBe('$123.01');
    expect(formatMoney(55555)).toBe('$555.55');
    expect(formatMoney(99999)).toBe('$999.99');
  });

  test('numbers 1000 and up should include commas as expected', () => {
    expect(formatMoney(1234567890)).toBe('$12,345,678.90');
    expect(formatMoney(111111)).toBe('$1,111.11');
  });

  test('null values should return -', () => {
    expect(formatMoney(null)).toBe('-');
  });

  test('NaN values should return -', () => {
    expect(formatMoney('lorem')).toBe('-');
  });
});
