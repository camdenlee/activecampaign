import { convertValue } from '../../utils/exchangeRate';

describe('test cases for convertValue()', () => {
  const rates = {
    AUD: 1.4,
    EUR: 0.85,
    JPY: 105,
  };

  test('Money should convert if the currency is in the table ', () => {
    expect(convertValue(1000, 'AUD', rates)).toBe(1400);
    expect(convertValue(12345, 'EUR', rates)).toBe(10493.25);
  });

  test('Money should convert even if currency is not uppercase', () => {
    expect(convertValue(20, 'jpy', rates)).toBe(2100);
    expect(convertValue(87, 'aud', rates)).toBe(121.8);
  });

  test('If the currency is not in the table, we should return 0', () => {
    expect(convertValue(20, 'abc', rates)).toBe(0);
    expect(convertValue(20, 'CBS', rates)).toBe(0);
  });
});
