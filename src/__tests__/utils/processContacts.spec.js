import { calculateValue } from '../../utils/processContacts';

describe('test cases for calculateValue()', () => {
  const rates = {
    AUD: 1.4,
    EUR: 0.85,
    JPY: 105,
    USD: 1,
  };

  const allDeals = {
    1: {
      value: '12345',
      currency: 'USD',
    },
    2: {
      value: '10000',
      currency: 'EUR',
    },
    3: {
      value: '67500',
      currency: 'USD',
    },
    4: {
      value: '11111',
      currency: 'AUD',
    },
    5: {
      value: '20000',
      currency: 'JPY',
    },
  };

  test('Contacts with deals in the array should convert and caluate', () => {
    expect(calculateValue([1, 3], allDeals, rates)).toBe('$798.45');
    expect(calculateValue([1, 2, 3, 4, 5], allDeals, rates)).toBe('$22,039.00');
  });

  test('Contacts without deals should return 0', () => {
    expect(calculateValue([], allDeals, rates)).toBe('$0.00');
  });
});
