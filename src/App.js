import React from 'react';
import Dashboard from './components/Dashboard';
import GlobalStyles from './components/GlobalStyles';

const App = () => (
  <>
    <GlobalStyles />
    <main>
      <div className="container">
        <h1>ActiveCampaign Coding Challenge</h1>
        <Dashboard />
      </div>
    </main>
  </>
);

export default App;
