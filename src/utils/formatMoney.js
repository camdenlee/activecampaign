const formatMoney = (number) => {
  if (typeof number !== 'number') {
    return '-';
  }

  return new Intl.NumberFormat('en-US', {
    minimumFractionDigits: 2,
    style: 'currency',
    currency: 'USD',
  }).format(number / 100); // all values from the API come in cents
};

export default formatMoney;
