const arrayToObject = (array, key) => (
  array.reduce((obj, item) => ({
    ...obj,
    [item[key]]: item,
  }), {})
);

export default arrayToObject;
