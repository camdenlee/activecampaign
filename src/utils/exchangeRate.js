import fetch from 'node-fetch';

const getExchangeRates = async () => {
  const endpoint = 'https://api.exchangeratesapi.io/latest?base=USD';
  const options = {
    method: 'get',
  };

  try {
    return await (await fetch(endpoint, options)).json();
  } catch (err) {
    /* eslint-disable-next-line no-console */
    console.log(err);
  }

  return {};
};

export default getExchangeRates;

export const convertValue = (value, from, rates) => {
  const fromCurrency = from.toUpperCase();

  if (rates[fromCurrency]) {
    return value * rates[fromCurrency];
  }

  /* eslint-disable-next-line no-console */
  console.log(`We don't have information for ${fromCurrency}`);
  return 0;
};
