const abbreviateName = (name) => name.split(' ').map((word) => word.charAt(0)).join('').substring(0, 2);

export default abbreviateName;
