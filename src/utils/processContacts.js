import arrayToObject from './arrayToObject';
import formatMoney from './formatMoney';
import getExchangeRates, { convertValue } from './exchangeRate';

export const calculateValue = (deals, allDeals, exchangeRates) => {
  const dealsArray = deals.map((deal) => ({
    value: allDeals[deal].value,
    currency: allDeals[deal].currency,
  }));

  const totalValueRaw = dealsArray.reduce((accumulator, current) => (
    accumulator + convertValue(parseFloat(current.value), current.currency, exchangeRates)
  ), 0);

  return formatMoney(totalValueRaw);
};

const processData = async (data) => {
  const exchangeRates = await getExchangeRates();

  const allContactTags = arrayToObject(data.contactTags, 'id');
  const allTags = arrayToObject(data.tags, 'id');
  const allDeals = arrayToObject(data.deals, 'id');

  const contacts = data.contacts.map(({
    firstName, lastName, id, deals, contactTags,
  }) => {
    const tags = contactTags.map((ct) => allTags[allContactTags[ct].tag].tag);
    const value = calculateValue(deals, allDeals, exchangeRates.rates);

    return (
      {
        name: `${firstName} ${lastName}`,
        location: '-', // See readme notes about location
        value,
        id,
        deals: deals.length,
        tags: tags.join(', '),
      }
    );
  });

  return contacts;
};

export default processData;
