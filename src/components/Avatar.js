import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { colors } from '../utils/variables';
import abbreviateName from '../utils/abbreviateName';

const { slate120, lavender40 } = colors;

const AvatarOuter = styled.div`
  height: 22px;
  width: 22px;
  background: ${lavender40};
  color: ${slate120};
  display: inline-block;
  border-radius: 50%;
  margin-right: 12px;
  line-height: 22px;
  text-align: center;
  font-size: 11px;
  font-weight: 600;
`;

const Avatar = ({ name }) => (
  <AvatarOuter>
    { abbreviateName(name) }
  </AvatarOuter>
);

Avatar.propTypes = {
  name: PropTypes.string.isRequired,
};

export default Avatar;
