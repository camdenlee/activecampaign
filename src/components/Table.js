import React from 'react';
import PropTypes from 'prop-types';
import { useTable, useFlexLayout } from 'react-table';
import styled from 'styled-components';
import { colors } from '../utils/variables';
import Checkbox from './Checkbox';

const { slate30, slate80 } = colors;

const StyledTable = styled.table`
  min-width: 100%;
  border: ${slate30} 1px solid;
  font-size: 14px;
  width: 100%;
  overflow-x: scroll;

  th {
    height: 28px;
    text-align: left;
    color: ${slate80};
    font-weight: 600;
    font-size: 12px;
    border-bottom: ${slate30} 1px solid;
    padding-left: 12px;
    display: flex;
    align-items: center;
  }

  td {
    height: 46px;
    border-bottom: ${slate30} 1px solid;
    padding-left: 12px;
    display: flex;
    align-items: center;
  }

  tbody {
    tr:last-of-type {
      td {
        border-bottom: 0;
      }
    }
  }
`;

export const TableHorizontalScroll = styled.div`
  overflow-x: scroll;
`;

const Table = ({ columns, data }) => {
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
  } = useTable(
    {
      columns,
      data,
    },
    useFlexLayout,
    (hooks) => {
      // This adds checkboxes but they won't work for this exercise
      /* eslint-disable react/prop-types,react/display-name */
      hooks.visibleColumns.push((currentColumns) => [
        {
          id: 'selection',
          Header: () => <Checkbox id="header" />,
          Cell: ({ row }) => <Checkbox id={row.id} />,
          width: 15,
        },
        ...currentColumns,
      ]);
      /* eslint-enable */
    },
  );

  const tableProps = getTableProps();
  const tableBodyProps = getTableBodyProps();

  return (
    <StyledTable role={tableProps.role} style={tableProps.style} cellSpacing={0} cellPadding={0}>
      <thead>
        {
          headerGroups.map((headerGroup) => {
            const headerGroupProps = headerGroup.getHeaderGroupProps();

            return (
              <tr
                key={headerGroupProps.key}
                role={headerGroupProps.role}
                style={headerGroupProps.style}
              >
                {
                  headerGroup.headers.map((column) => {
                    const headerProps = column.getHeaderProps();

                    return (
                      <th
                        key={headerProps.key}
                        role={headerProps.role}
                        colSpan={headerProps.colSpan}
                        style={headerProps.style}
                      >
                        {column.render('Header')}
                      </th>
                    );
                  })
                }
              </tr>
            );
          })
        }
      </thead>
      <tbody role={tableBodyProps.role}>
        {rows.map((row) => {
          prepareRow(row);
          const rowProps = row.getRowProps();

          return (
            <tr key={rowProps.key} role={rowProps.role} style={rowProps.style}>
              {
                row.cells.map((cell) => {
                  const cellProps = cell.getCellProps();

                  return (
                    <td key={cellProps.key} role={cellProps.role} style={cellProps.style}>
                      {cell.render('Cell')}
                    </td>
                  );
                })
              }
            </tr>
          );
        })}
      </tbody>
    </StyledTable>
  );
};

Table.propTypes = {
  columns: PropTypes.arrayOf(
    PropTypes.object,
  ).isRequired,
  data: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      location: PropTypes.string.isRequired,
      value: PropTypes.string.isRequired,
      id: PropTypes.string.isRequired,
      deals: PropTypes.number.isRequired,
      tags: PropTypes.string.isRequired,
    }),
  ).isRequired,
};

export default Table;
