import React from 'react';
import styled, { keyframes } from 'styled-components';
import { colors } from '../utils/variables';

const { ocean, ocean60 } = colors;

const spin = keyframes`

  to {
    transform: rotate(360deg);
  }
`;

const LoadingIcon = styled.div`
  width: 50px;
  height: 50px;
  border-radius: 50%;
  display: block;
  border: 5px solid ${ocean60};
  border-top-color: ${ocean};
  animation: ${spin} 1.5s ease-in-out infinite;
  margin: 100px auto;
`;

const Loading = () => (
  <LoadingIcon role="status">
    <span className="sr-only">Loading...</span>
  </LoadingIcon>
);

export default Loading;
