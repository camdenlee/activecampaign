/*
 * I created this simple component for the visual impact on the table. The functionality currently
 * does not work. I did ensure that accessibility best practices are followed so that a11y linters
 * and automated checks pass.
 */
import React from 'react';
import PropTypes from 'prop-types';

const Checkbox = ({ id }) => {
  const handleClick = () => {
    /* eslint-disable-next-line no-console */
    console.log('You clicked the checkbox, but the scope of this exercise does not include row selection.');
  };

  return (
    <>
      <label className="sr-only" htmlFor={`checkbox-${id}`}>Select this row</label>
      <input type="checkbox" id={`checkbox-${id}`} onChange={() => handleClick()} />
    </>
  );
};

Checkbox.propTypes = {
  id: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]).isRequired,
};

export default Checkbox;
