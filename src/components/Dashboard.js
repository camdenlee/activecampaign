import React, { useState, useEffect, useMemo } from 'react';
import fetch from 'node-fetch';
import Table, { TableHorizontalScroll } from './Table';
import processData from '../utils/processContacts';
import Avatar from './Avatar';
import Loading from './Loading';

const Dashboard = () => {
  const [dashboardData, setDashboardData] = useState({});
  const [isLoading, setLoading] = useState(true);

  const makeRequest = async () => {
    setLoading(true);

    const endpoint = `${process.env.REACT_APP_ENDPOINT}contacts?limit=50&include=contactTags.tag,deals`;
    const options = {
      method: 'get',
      headers: {
        'Api-Token': process.env.REACT_APP_API_TOKEN,
      },
    };

    try {
      const response = await (await fetch(endpoint, options)).json();
      setDashboardData(await processData(response));
      setLoading(false);
    } catch (err) {
      /* eslint-disable-next-line no-console */
      console.log(err);
    }
  };

  useEffect(() => {
    makeRequest();
  }, []);

  const columns = useMemo(() => [
    {
      Header: 'Contact',
      accessor: 'name',
      width: 120,
      /* eslint-disable react/prop-types,react/display-name */
      Cell: ({ value, row }) => (
        <>
          <Avatar name={value} />
          <a
            href={`${process.env.REACT_APP_BASE_URL}app/contacts/${row.original.id}`}
            target="_blank"
            rel="noreferrer"
          >
            { value }
          </a>
        </>
      ),
      /* eslint-enable */
    },
    {
      Header: 'Total Value',
      accessor: 'value',
      width: 80,
    },
    {
      Header: 'Location',
      accessor: 'location',
      width: 60,
    },
    {
      Header: 'Deals',
      accessor: 'deals',
      width: 40,
    },
    {
      Header: 'Tags',
      accessor: 'tags',
    },
  ], []);

  return (
    <>
      {
        isLoading ? (
          <Loading />
        ) : (
          <TableHorizontalScroll>
            <Table columns={columns} data={dashboardData} />
          </TableHorizontalScroll>
        )
      }
    </>
  );
};

export default Dashboard;
