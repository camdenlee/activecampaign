import { createGlobalStyle } from 'styled-components';
import 'typeface-poppins/index.css';
import 'typeface-ibm-plex-sans/index.css';
import 'normalize.css';
import { colors, fontFamily } from '../utils/variables';

const { ibmplex, poppins } = fontFamily;
const { slate120, ocean, ocean120 } = colors;

const GlobalStyles = createGlobalStyle`
  * {
    box-sizing: border-box;
  }

  body {
    font-family: ${ibmplex};
    font-size: 16px;
    line-height: 1.25;
    color: ${slate120};
  }

  h1,
  h2,
  h3,
  h4,
  h5,
  h6 {
    font-family: ${poppins};
    line-height: 1.3;
  }

  /* Simple h1 styles for the presentation, but not based on style guide */
  h1 {
    text-align: center;
    font-size: 48px;
    line-height: 1.1666;
    margin: 0 auto 50px;

    @media (max-width: 767px) {
      font-size: 36px;
    }
    @media (max-width: 374px) {
      font-size: 30px;
    }
  }

  p {
    color: $slate-80;

    strong {
      color: $slate-120;
    }
  }

  a {
    color: ${ocean};
    text-decoration: none;
    font-weight: 500;
    transition: 0.3s;

    &:hover,
    &:focus {
      color: ${ocean120};
    }
  }

  /* Simple style for the app container */
  .container {
    width: 100%;
    max-width: 1200px;
    margin: 0 auto;
    padding: 50px 30px 100px;
  }

  .sr-only {
    position: absolute;
    width: 1px;
    height: 1px;
    padding: 0;
    margin: -1px;
    overflow: hidden;
    clip: rect(0, 0, 0, 0);
    white-space: nowrap;
    border: 0;
  }
`;

export default GlobalStyles;
