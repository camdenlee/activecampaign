module.exports = {
  "roots": [
    "<rootDir>/src"
  ],
  "testMatch": [
    "<rootDir>/src/**/__tests__/**/*.{js,jsx,ts,tsx}",
    "<rootDir>/src/**/*.{spec,test}.{js,jsx,ts,tsx}"
  ],
  "moduleNameMapper": {
    "\\.svg": "@svgr/webpack"
  },
};
